// Aidan holmes - 2042141
package application;
import vehicles.Bicycle;;
public class BikeStore{
    
    public static void main(String[] args){

        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Raythean", 34, 50);
        bikes[1] = new Bicycle("Blacksteel", 41, 57);
        bikes[2] = new Bicycle("Joe from down the street", 3, 12.3);
        bikes[3] = new Bicycle("Hephaestus, God of the forge", 1, 999999999999999999.0);


        for (Bicycle i : bikes) {
            System.out.println(i);
        }

    }

}
